import React from 'react';
import {ActivityIndicator, ScrollView, StyleSheet} from 'react-native';
import {chunk} from 'lodash';
import {Col, Grid} from 'react-native-easy-grid';
import ArtistElement from '../components/ArtistElement';
import {Container, Row} from 'native-base';
import {useSelector} from 'react-redux';
import {IArtist} from '../store/modules/dataReducer';
import {IReducers} from '../store/modules';

const styles = StyleSheet.create({
    root: {paddingTop: 4},
    indicatorContainer: {display: 'flex', justifyContent: 'center', alignItems: 'center'},
    indicator: {display: 'flex'},
});

const HomeLayout: React.FC = () => {
    const artists = useSelector<IReducers, IArtist[]>((state) => state.data.artists);

    return (
        <Container style={styles.root}>
            <Grid>
                {
                    artists.length === 0
                        ? <Row style={styles.indicatorContainer}>
                            <ActivityIndicator
                                style={styles.indicator}
                                size={64}
                            />
                        </Row>
                        : <ScrollView>
                            {
                                chunk<IArtist>(artists, 3).map((artistChunk, index) => (
                                    <Row key={`artistChunk_${index}`}>
                                        <Col>{artistChunk[0] && <ArtistElement {...artistChunk[0]}/>}</Col>
                                        <Col>{artistChunk[1] && <ArtistElement {...artistChunk[1]}/>}</Col>
                                        <Col>{artistChunk[2] && <ArtistElement {...artistChunk[2]}/>}</Col>
                                    </Row>
                                ))
                            }
                        </ScrollView>
                }
            </Grid>
        </Container>
    );
};

export default HomeLayout;
