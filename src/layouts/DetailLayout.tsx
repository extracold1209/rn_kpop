import React, {useState} from 'react';
import {Container} from 'native-base';
import {ScrollView, StyleSheet} from 'react-native';
import {useSelector} from 'react-redux';
import {IReducers} from '../store/modules';
import {IArtist} from '../store/modules/dataReducer';
import YoutubeElement from '../components/YoutubeElement';
import DetailArtistInfoRowSection from '../sections/DetailArtistInfoRowSection';
import DetailSongListRowSection from '../sections/DetailSongListRowSection';

const styles = StyleSheet.create({
    songListContainer: {
        flexDirection: 'column',
    },
});

const DetailLayout: React.FC = () => {
    const artist = useSelector<IReducers, IArtist | undefined>((state) => state.data.selectedArtist);
    const [selectedSongId, selectSongId] = useState<string | undefined>(undefined);
    if (!artist) {
        console.warn('artist is undefined');
        return <Container/>;
    }

    const {songs} = artist;

    return (
        <Container>
            {selectedSongId && <YoutubeElement videoId={selectedSongId}/>}
            <ScrollView>
                <DetailArtistInfoRowSection artist={artist}/>
                <DetailSongListRowSection songs={songs} onClick={(youtubeId) => {
                    selectSongId(youtubeId);
                }}/>
            </ScrollView>
        </Container>
    );
};

export default DetailLayout;
