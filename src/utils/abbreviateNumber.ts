const SI_SYMBOL = ['', 'k', 'M', 'G', 'T', 'P', 'E'];
// https://stackoverflow.com/a/40724354/10083639
function abbreviateNumber(number: number) {

    // what tier? (determines SI symbol)
    const tier = Math.log10(number) / 3 | 0;

    // if zero, we don't need a suffix
    // eslint-disable-next-line eqeqeq
    if (tier == 0) {
        return number;
    }

    // get suffix and determine scale
    const suffix = SI_SYMBOL[tier];
    const scale = Math.pow(10, tier * 3);

    // scale the number
    const scaled = number / scale;

    // format number and add suffix
    return scaled.toFixed(1) + suffix;
}


export default abbreviateNumber;
