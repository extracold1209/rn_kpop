import {produce} from 'immer';
// Actions
import {AnyAction, Reducer} from 'redux';

// interfaces
export type IHelloReducer = {
    text: string
}


export const HELLO = 'hello/HELLO';

// Action Functions
export const helloFunction: AnyAction = {type: HELLO};

// Default State
const defaultState: IHelloReducer = {
    text: 'hello',
};

// Reducer
const helloReducer: Reducer<IHelloReducer> = (state = defaultState, action) => {
    switch (action.type) {
    case HELLO:
        return produce(state, (draft) => {
            draft.text = 'world!';
        });
    default:
        return state;
    }
};

export default helloReducer;
