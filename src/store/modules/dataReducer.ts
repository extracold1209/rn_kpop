// Actions
import {Reducer} from 'redux';
import produce from 'immer';
import {FirebaseFirestoreTypes} from '@react-native-firebase/firestore';

// interfaces
export type IArtist = {
    name: string;
    agency: string;
    members: string[];
    debut: string;
    twitterId?: string;
    vLiveId?: string;
    thumbnailImage: string;
    songs: ISong[]
}

export type IConvertedSong = {
    name: string;
    youtubeId: string;
    youtubeViewCount: number;
    youtubePublishAt: string;
    clickCount: number;
    playCount: number;
    thumbnailUrl: string;
}

export type ISong = IConvertedSong | FirebaseFirestoreTypes.DocumentReference;

export type IDataReducer = {
    artists: IArtist[],
    selectedArtist?: IArtist;
}


export const ARTIST_LIST_REFRESH_REQUESTED = 'data/ARTIST_LIST_REFRESH_REQUESTED';
export const ARTIST_LIST_CHANGED = 'data/ARTIST_LIST_CHANGED';
export const ARTIST_SELECTED = 'data/ARTIST_SELECTED';
export const SONG_CONVERTED_ARTIST_SELECTED = 'data/SONG_CONVERTED_ARTIST_SELECTED';

// Action Functions
export const refreshArtistList = () => ({type: ARTIST_LIST_REFRESH_REQUESTED});
export const changeArtistList = (artist: IArtist[]) => ({type: ARTIST_LIST_CHANGED, payload: artist});
export const selectArtist = (artist: IArtist) => ({type: ARTIST_SELECTED, payload: artist});
export const selectSongConvertedArtist = (artist: IArtist) => ({type: SONG_CONVERTED_ARTIST_SELECTED, payload: artist});

// Default State
const defaultState: IDataReducer = {
    artists: [],
    selectedArtist: undefined,
};

// Reducer
const dataReducer: Reducer<IDataReducer> = (state = defaultState, action) => {
    switch (action.type) {
    case ARTIST_LIST_CHANGED:
        return produce(state, (draft) => {
            draft.artists = action.payload;
        });
    case SONG_CONVERTED_ARTIST_SELECTED:
    case ARTIST_SELECTED: {
        return produce(state, (draft) => {
            draft.selectedArtist = action.payload;
        });
    }
    default:
        return state;
    }
};

export default dataReducer;
