import {Action, combineReducers} from 'redux';
import {enableES5} from 'immer';
import helloReducer, {IHelloReducer} from './helloReducer';
import dataReducer, {IDataReducer} from './dataReducer';

enableES5();

export type IReduxAction<P, T> = (payload: P) => Action<T> & { payload: P }

export function makeAction<P, T>(type: T): IReduxAction<P, T> {
    return (payload: P) => ({type, payload});
}

export type IReducers = {
    hello: IHelloReducer,
    data: IDataReducer,
}

export default combineReducers<IReducers>({
    hello: helloReducer,
    data: dataReducer,
});
