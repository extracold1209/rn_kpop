import {call, put, takeEvery} from 'redux-saga/effects';
import {chunk, cloneDeep, flatten} from 'lodash';
import {Action} from 'redux';
import {
    ARTIST_LIST_REFRESH_REQUESTED,
    ARTIST_SELECTED,
    changeArtistList,
    IArtist,
    ISong,
    selectSongConvertedArtist,
} from '../modules/dataReducer';
import firestore, {FirebaseFirestoreTypes} from '@react-native-firebase/firestore';

type Timestamp = FirebaseFirestoreTypes.Timestamp;
type DocSnapshot = FirebaseFirestoreTypes.DocumentSnapshot;
type DocReference = FirebaseFirestoreTypes.DocumentReference;

const convertSongCollectionToJs = async (song: any) => {
    const songData = song.data();
    songData.youtubePublishAt = (songData.youtubePublishAt as Timestamp).toDate().toLocaleDateString();
    return songData as ISong;
};

const convertArtistCollectionToJs = async (doc: DocSnapshot) => {
    const artist = doc.data() as any;

    artist.debut = (artist.debut as Timestamp).toDate().toISOString().replace(/T.*/, '');
    if (artist.songs) {
        // artist.songs = await Promise.all((artist.songs as DocReference[]).map(async (song) => convertSongCollectionToJs(song.get())));
        // artist.songs = [];
    }
    return artist as IArtist;
};

async function getArtistList() {
    const store = await firestore();
    const artistCollection = store.collection('artists');
    const snapshot = await artistCollection.get();
    return await Promise.all(snapshot.docs.map(convertArtistCollectionToJs));
}

async function getSongConvertedArtist(artist: IArtist): Promise<ISong[]> {
    const store = await firestore();
    const songsCollection = store.collection('songs');

    const convertedSongChunks = await Promise.all(chunk<DocReference>(artist.songs as DocReference[], 10).map(async (songChunk) => {
        const docSongs = await songsCollection.where(firestore.FieldPath.documentId(), 'in', songChunk.map((song) => song.id)).get();
        return await Promise.all(docSongs.docs.map(convertSongCollectionToJs));
    }));

    return flatten(convertedSongChunks);
}

function* handleRefreshArtists() {
    try {
        const updatedDocs: IArtist[] = yield call(getArtistList);
        yield put(changeArtistList(updatedDocs));
    } catch (e) {
        // alert('목록 불러오기가 실패했습니다.');
        console.error(e);
    }
}

// 곡 목록을 변환해주어야 한다
function* handleSelectArtist({payload}: Action & { payload: IArtist }) {
    const convertedArtist = cloneDeep(payload);
    convertedArtist.songs = yield call(getSongConvertedArtist, convertedArtist);
    console.log('convertedArtist', convertedArtist);
    yield put(selectSongConvertedArtist(convertedArtist));
}

export default function* () {
    yield takeEvery(ARTIST_LIST_REFRESH_REQUESTED, handleRefreshArtists);
    yield takeEvery(ARTIST_SELECTED, handleSelectArtist);
    // yield takeEvery(ARTIST_ADDED, handleAddArtist);
}
