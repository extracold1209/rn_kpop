import React from 'react';
import {Text, Thumbnail} from 'native-base';
import {PixelRatio, StyleSheet, TouchableOpacity} from 'react-native';
import {IArtist, selectArtist} from '../store/modules/dataReducer';
import {useNavigation} from '@react-navigation/native';
import {useDispatch} from 'react-redux';

const nameTextFontSize = PixelRatio.get() < 2 ? 14 : 16;

const styles = StyleSheet.create({
    root: {
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 4,
    },
    thumbnail: {
        width: 110,
        height: 110,
        borderRadius: 60,
        marginBottom: 5,
    },
    nameText: {
        fontSize: nameTextFontSize,
    },
});

type IProps = any | IArtist;

const ArtistElement: React.FC<IProps> = (props) => {
    const {thumbnailImage, name} = props;
    const navigator = useNavigation();
    const dispatch = useDispatch();

    return (
        <TouchableOpacity style={styles.root} onPress={() => {
            if (thumbnailImage && name) {
                dispatch(selectArtist(props));
                navigator.navigate('stack/Detail');
            }
        }}>
            <Thumbnail large source={{uri: thumbnailImage}} style={styles.thumbnail}/>
            <Text style={styles.nameText}>{name}</Text>
        </TouchableOpacity>
    );
};

export default ArtistElement;
