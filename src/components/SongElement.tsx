import {Image, StyleSheet, TouchableOpacity} from 'react-native';
import {Card, CardItem, Icon, Text, View} from 'native-base';
import TextTicker from 'react-native-text-ticker';
import React from 'react';
import {ISong} from '../store/modules/dataReducer';
import abbreviateNumber from '../utils/abbreviateNumber';

type IProps = {
    song: ISong;
    onClick: (youtubeId: string) => void;
};

const styles = StyleSheet.create({
    root: {},
    imageContainer: {paddingLeft: 16, paddingRight: 16, paddingTop: 4, paddingBottom: 4, width: '100%', height: 120},
    imageElement: {height: '100%'},
    imageOverContainer: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        flexDirection: 'row',
    },
    imageOverSection: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        backgroundColor: 'rgba(52, 52, 52, 0.3)',
        marginRight: 5,
    },
    imageOverIcon: {
        top: 2,
        marginRight: 4,
        fontSize: 16,
        color: 'white',
    },
    imageOverText: {
        fontSize: 14,
        color: 'white',
        justifyContent: 'flex-end',
    },
    songNameText: {
        color: 'white',
    },
});

const SongElement: React.FC<IProps> = (props) => {
    const {song, onClick} = props;
    const {thumbnailUrl, youtubeViewCount} = song;

    return (
        <Card style={styles.root}>
            <TouchableOpacity
                style={{backgroundColor: 'transparent'}}
                onPress={() => {
                    onClick(song.youtubeId);
                }}
            >
                <CardItem cardBody>
                    <View style={styles.imageContainer}>
                        <Image style={styles.imageElement} source={{uri: thumbnailUrl}}/>
                    </View>
                    <View style={styles.imageOverContainer}>
                        <View style={styles.imageOverSection}>
                            <Icon style={styles.imageOverIcon} type="FontAwesome" name="eye"/>
                            <Text style={styles.imageOverText}>{abbreviateNumber(youtubeViewCount)}</Text>
                        </View>
                    </View>
                </CardItem>
                <CardItem>
                    <TextTicker
                        style={styles.songNameText}
                        duration={song.name.length * 200}
                        bounce={false}
                    >
                        {song.name}
                    </TextTicker>
                </CardItem>
            </TouchableOpacity>
        </Card>
    );
};

export default SongElement;
