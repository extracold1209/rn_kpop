import React, {useEffect} from 'react';
import {refreshArtistList} from '../store/modules/dataReducer';
import {useDispatch} from 'react-redux';

export default () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(refreshArtistList());
    }, [dispatch]);

    return <></>;
};
