import React from 'react';
import {StyleSheet} from 'react-native';
import YouTube from 'react-native-youtube';
import {useIsFocused} from '@react-navigation/native';

const styles = StyleSheet.create({
    youtubeContainer: {alignSelf: 'stretch', height: 300},
});

type IProps = {
    videoId: string;
}

const YoutubeElement: React.FC<IProps> = (props) => {
    const isFocused = useIsFocused();

    return (
        <>
            {isFocused &&
            <YouTube
                apiKey={'AIzaSyCgNUn-9OkTyA9rW4U-sXGh5lJ2_jQGmSI'}
                videoId={props.videoId} // The YouTube video ID
                play // control playback of video with true/false
                loop // control whether the video should loop when ended
                onReady={() => {
                    console.log('onReady');
                }}
                // onChangeState={e => this.setState({ status: e.state })}
                // onChangeQuality={e => this.setState({ quality: e.quality })}
                onError={console.error}
                style={styles.youtubeContainer}
            />
            }
        </>
    );
};

export default YoutubeElement;
