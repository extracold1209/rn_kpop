import {StatusBar} from 'react-native';
import React from 'react';

export default () => <StatusBar
    backgroundColor={'#121212'}
    barStyle={'default'}
/>;

