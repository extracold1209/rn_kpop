import React from 'react';
import {Col, Grid, Row} from 'react-native-easy-grid';
import {Image, Linking, StyleSheet} from 'react-native';
import {Card, CardItem, H2, Icon, Text} from 'native-base';
import {IArtist} from '../store/modules/dataReducer';

const styles = StyleSheet.create({
    imageContainer: {
        paddingTop: 8,
        paddingHorizontal: 8,
    },
    image: {
        width: '100%',
        minHeight: 150,
        resizeMode: 'contain',
    },
    infoContainer: {},
    infoElement: {
        flexDirection: 'column',
        justifyContent: 'center',
    },
});

type IProps = {
    artist: IArtist
}

const DetailArtistInfoRowSection: React.FC<IProps> = (props) => {
    const artist = props.artist;
    const {twitterId, vLiveId, name, agency, members, debut} = artist;

    return (
        <Card>
            <CardItem cardBody style={styles.imageContainer}>
                <Image style={styles.image} source={{uri: artist.thumbnailImage}}/>
            </CardItem>
            <CardItem cardBody>
                <Grid>
                    <Col style={styles.imageContainer}>
                        <Row style={styles.infoElement}>
                            <H2>{name}</H2>
                        </Row>
                        <Row style={styles.infoElement}>
                            <Text>Agency | {agency}</Text>
                        </Row>
                        <Row style={styles.infoElement}>
                            <Text>Members | {members && members.join(', ')}</Text>
                        </Row>
                        <Row style={styles.infoElement}>
                            <Text>Debut | {debut}</Text>
                        </Row>
                    </Col>
                </Grid>
            </CardItem>
            {
                (twitterId || vLiveId) &&
                <CardItem footer>
                    {twitterId && <Icon onPress={() => {
                        Linking.openURL(`https://twitter.com/${twitterId}`);
                    }} type={'FontAwesome'} name={'twitter'} style={{top: 4, marginRight: 4, color: '#38A1F3'}}/>}
                    {vLiveId && <Icon onPress={() => {
                        Linking.openURL(`https://channels.vlive.tv/${vLiveId}`);
                    }} type={'FontAwesome'} name={'vimeo'} style={{top: 4, marginRight: 4, color: '#54f7ffEE'}}/>}
                </CardItem>
            }
        </Card>
    );
};

export default DetailArtistInfoRowSection;
