import React from 'react';
import {Col, Row} from 'react-native-easy-grid';
import {Text} from 'native-base';
import {ActivityIndicator, ScrollView, StyleSheet} from 'react-native';
import SongElement from '../components/SongElement';
import {IConvertedSong, ISong} from '../store/modules/dataReducer';

const styles = StyleSheet.create({
    songListContainer: {
        flexDirection: 'column',
        paddingLeft: 4,
        paddingRight: 4,
    },
    songListTitleContainer: {padding: 4, justifyContent: 'center'},
    songListTitle: {paddingTop: 4, paddingBottom: 4, fontSize: 20},
});

type IProps = {
    songs: ISong[];
    onClick: (youtubeId: string) => void;
}

const DetailSongListRowSection: React.FC<IProps> = (props) => {
    const songs = props.songs;
    console.log('DetailSongListRowSection', songs);
    const filteredSongs = songs.filter((song: any) => song.youtubeId) as IConvertedSong[];

    console.log('filterted', filteredSongs);
    return (
        <Row style={styles.songListContainer}>
            {/* Songs Field*/}
            <Col size={1} style={styles.songListTitleContainer}>
                <Text style={styles.songListTitle}>{'Song List'}</Text>
            </Col>
            <Col size={3}>
                <ScrollView>
                    {
                        filteredSongs.length === 0
                            ? <ActivityIndicator size={64}/>
                            : filteredSongs.map((song) => {
                                return <SongElement
                                    key={song.youtubeId}
                                    song={song}
                                    onClick={(youtubeId) => {
                                        props.onClick(youtubeId);
                                    }}
                                />;
                            })}
                </ScrollView>
            </Col>
        </Row>
    );
};

export default DetailSongListRowSection;
