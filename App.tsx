import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Root, StyleProvider, View} from 'native-base';
import store from './src/store';
import CustomStatusBar from './src/components/StatusBar';
import {Provider} from 'react-redux';
import DetailLayout from './src/layouts/DetailLayout';
import HomeLayout from './src/layouts/HomeLayout';
import getTheme from './src/native-base-theme/components';
import material from './src/native-base-theme/variables/material';
import FireStoreWatcher from './src/components/FireStoreWatcher';
import {BannerAd, BannerAdSize} from '@react-native-firebase/admob';

const Stack = createStackNavigator();

const App = () => {
    return (
        <NavigationContainer>
            <Provider store={store}>
                <StyleProvider style={getTheme(material)}>
                    <Root>
                        <CustomStatusBar/>
                        <Stack.Navigator
                            initialRouteName={'Home'}
                            headerMode={'screen'}
                        >
                            <Stack.Screen
                                name={'stack/Home'}
                                options={{
                                    title: 'K-POP Artists',
                                    headerStyle: {
                                        backgroundColor: '#121212d6',
                                    },
                                    headerTintColor: '#fff',
                                    headerTitleStyle: {
                                        fontWeight: 'bold',
                                    },
                                }}
                                component={() => <View>
                                    <HomeLayout/>
                                    <BannerAd
                                        unitId={'ca-app-pub-3920770526931151/7016638522'}
                                        size={BannerAdSize.FULL_BANNER}
                                        requestOptions={{
                                            testDevices: ['EMULATOR'],
                                        }}
                                        onAdLoaded={() => {
                                            console.log('Advert loaded');
                                        }}
                                        onAdFailedToLoad={(error: Error) => {
                                            console.log('Advert failed to load: ', error);
                                        }}
                                    />
                                </View>}
                            />
                            <Stack.Screen
                                name={'stack/Detail'}
                                options={{
                                    title: 'Detail',
                                    headerStyle: {
                                        backgroundColor: '#121212d6',
                                    },
                                    headerTintColor: '#fff',
                                    headerTitleStyle: {
                                        fontWeight: 'bold',
                                    },
                                }}
                                component={DetailLayout}
                            />
                        </Stack.Navigator>
                    </Root>
                </StyleProvider>
                <FireStoreWatcher/>
            </Provider>
        </NavigationContainer>
    );
};

export default App;
