import {Dimensions, Platform} from 'react-native';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const platform = Platform.OS;
const platformStyle = 'material';
const isIphoneX =
    platform === 'ios' && deviceHeight === 812 && deviceWidth === 375;

export default {
    'platformStyle': 'material',
};
