/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import fireStoreApp from '@react-native-firebase/app';

(async () => {
    if (fireStoreApp.apps.length === 0) {
        await fireStoreApp.initializeApp({
            apiKey: 'AIzaSyD7N0Je4vr6S8ozxRGFa7v4LSPloAIhdDw',
            authDomain: 'rn-kpop.firebaseapp.com',
            databaseURL: 'https://rn-kpop.firebaseio.com',
            projectId: 'rn-kpop',
            storageBucket: 'rn-kpop.appspot.com',
            messagingSenderId: '560624824164',
            appId: '1:560624824164:web:a0dc111b3e16e25703d471',
            measurementId: 'G-1ZXBP6M3H1',
        });
    }
})();

AppRegistry.registerComponent(appName, () => App);
