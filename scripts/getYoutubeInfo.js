const axios = require('axios');
const fs = require('fs');

const videoIds = process.argv[2].split(',');
console.log(process.argv);

const key = 'AIzaSyAA9seLWCTVrdZxLH5rtwCndqMb1xnGSDU';
const part = 'snippet,statistics';
const baseUrl = 'https://www.googleapis.com/youtube/v3/videos';

(async () => {
    const result = await Promise.all(videoIds.map(async (videoId) => {
        const { data } = await axios.request({
            url: baseUrl,
            method: 'get',
            params: {
                part,
                id: videoId,
                key,
            },
        });
        const video = data.items[0];

        const { snippet, statistics } = video;

        const { publishedAt, title, thumbnails } = snippet;
        const { medium: defaultThumbnail } = thumbnails;
        const { url: thumbnailUrl } = defaultThumbnail;

        const { viewCount, likeCount } = statistics;

        return {
            name: title,
            youtubeId: videoId,
            youtubeViewCount: Number.parseInt(viewCount, 10),
            youtubePublishAt: publishedAt,
            clickCount: 0,
            playCount: 0,
            thumbnailUrl,
        };
    }));

    fs.writeFileSync('./songs.json', JSON.stringify(result, undefined, 4));
})();
